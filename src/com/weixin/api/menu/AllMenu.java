package com.weixin.api.menu;

/**
 * 有个性化菜单时查询菜单结果
 * @author 张超
 * @date 2016年08月29日
 */
public class AllMenu {
    private NormalMenu menu;
    private ActiveConditionalMenu[] conditionalMenu;

    public NormalMenu getMenu() {
        return menu;
    }

    public void setMenu(NormalMenu menu) {
        this.menu = menu;
    }

    public ActiveConditionalMenu[] getConditionalMenu() {
        return conditionalMenu;
    }

    public void setConditionalMenu(ActiveConditionalMenu[] conditionalMenu) {
        this.conditionalMenu = conditionalMenu;
    }
}
