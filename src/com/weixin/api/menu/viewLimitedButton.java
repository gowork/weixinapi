package com.weixin.api.menu;

/**
 * 微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL
 * 永久素材类型只支持图文消息
 * @author 张超
 * @date 2016年08月29日
 */
public class viewLimitedButton extends Button {
    private String type;
    private String media_id;
    public viewLimitedButton(){this.setType("view_imited");}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }
}
