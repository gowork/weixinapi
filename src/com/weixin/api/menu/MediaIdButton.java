package com.weixin.api.menu;

/**
 * 微信服务器会将开发者填写的永久素材id对应的素材下发给用户
 * 永久素材可以是图片、音频、视频、图文消息
 * @author 张超
 * @date 2016年08月29日
 */
public class MediaIdButton extends Button{
    private String type;
    private String media_id;
    public MediaIdButton(){this.setType("media_id");}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }
}
