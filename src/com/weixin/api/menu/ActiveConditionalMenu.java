package com.weixin.api.menu;

/**
 * 有个性化菜单时查询的个性化菜单
 * @author 张超
 * @date 2016年08月29日
 */
public class ActiveConditionalMenu extends ConditionalMenu{
    private String menuid;

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }
}
