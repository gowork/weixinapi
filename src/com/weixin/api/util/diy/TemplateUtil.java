package com.weixin.api.util.diy;

import com.weixin.api.message.template.TemplateMessage;
import net.sf.json.JSONObject;

/**
 * 模版消息
 * @author 张超
 * @date 2016年08月29日
 */
public class TemplateUtil {
    private final static String SEND_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
    /**
     * 发送模版消息
     * @param accessToken
     * @param jsonStr 模版消息数据
     * @return
     */
    public static boolean sendTemplateMessage(String accessToken,String jsonStr){
        JSONObject jsonObject = CommonUtil.httpsRequest(SEND_TEMPLATE_URL.replace("ACCESS_TOKEN", accessToken),"POST",jsonStr);
        return jsonObject!=null && "ok".equals(jsonObject.getString("errmsg"));
    }
    /**
     * 发送模版消息
     * @param accessToken
     * @param templateMessage 模版消息数据
     * @return
     */
    public static boolean sendTemplateMessage(String accessToken, TemplateMessage templateMessage){
        return sendTemplateMessage(accessToken,JSONObject.fromObject(templateMessage).toString());
    }

    public static boolean delTemplate(String accessToken,String template_id){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("template_id",template_id);
//        jsonObject.
        return false;
    }
}
