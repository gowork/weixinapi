package com.weixin.api.message.template;

/**
 * 模版消息单项
 * @author 张超
 * @date 2016年09月02日
 */
public class DataItem {
    private String value;
    private String color;
    public DataItem(){
        this.color = "#173177";
    }
    public DataItem(String value){
        this.value = value;
        this.color = "#173177";
    }
    public DataItem(String value,String color){
        this.value = value;
        this.color = color;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
