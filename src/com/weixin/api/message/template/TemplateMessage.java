package com.weixin.api.message.template;

import java.util.Map;

/**
 * @author 张超
 * @date 2016年09月02日
 */
public class TemplateMessage {
    private String touser;
    private String template_id;
    private String url;
    private Map<String,DataItem> data;

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, DataItem> getData() {
        return data;
    }

    public void setData(Map<String, DataItem> data) {
        this.data = data;
    }
}
