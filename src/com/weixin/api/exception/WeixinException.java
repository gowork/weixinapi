package com.weixin.api.exception;

/**
 * 微信错误
 * @author 张超
 * @date 2016年08月29日
 */
public class WeixinException extends Exception {
    private static final long serialVersionUID = -6357149550353160810L;
    private WxError error;
    public WeixinException(WxError error) {
        super(error.toString());
        this.error = error;
    }

    public WxError getError() {
        return error;
    }

    public void setError(WxError error) {
        this.error = error;
    }
}
